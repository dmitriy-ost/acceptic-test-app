'use strict';

angular.
  module('fileDetails').
  component('fileDetails', {
    templateUrl: 'file-details/file-details.template.html',
    controller: ['$scope', 'selectedFile', 'dateFormat',
      function($scope, selectedFile, dateFormat) {
        $scope.selectedFile = selectedFile;

        $scope.showProperty = function(value) {
          return value instanceof Date || typeof value !== 'object';
        };

        $scope.prepareValue = function(key, value) {
          return key === 'createdAt' ? moment(value).format(dateFormat) : value;
        };
      }
    ]
  });
