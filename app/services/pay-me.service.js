'use strict';

angular.
  module('services').
  factory('payMe', ['$http',
    function($http) {
      return {
        makePayment: function(price) {
          return $http({
            method: 'POST',
            url: 'https://preprod.paymeservice.com/api/generate-sale',
            data: {
              seller_payme_id: 'MPL14985-68544Z1G-SPV5WK2K-0WJWHC7N',
              sale_price: price,
              currency: 'USD',
              product_name: 'Payment for files',
              installments: 1,
              language: 'en'
            }
          });
        }
      };
    }
  ]);
