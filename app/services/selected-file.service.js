'use strict';

angular.
  module('services').
  factory('selectedFile', [function() {
    var selectedFile;

    function setFile(file) {
      selectedFile = file;
    }

    function getFile() {
      return selectedFile;
    }

    return {
      setFile: setFile,
      getFile: getFile
    };
  }]);
