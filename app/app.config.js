'use strict';

angular.
  module('accepticTestApp').

  constant('appName', 'accepticTestApp').

  config(['localStorageServiceProvider', 'appName',
    function(localStorageServiceProvider, appName) {
      localStorageServiceProvider.setPrefix(appName);
    }
  ]);
