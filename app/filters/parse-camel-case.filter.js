'use strict';

angular.
  module('filters').
  filter('parseCamelCase', [function() {
    return function(input) {
      return input ? input.
        replace(/([A-Z])/g, ' $1').
        replace(/^./, function(str) {
          return str.toUpperCase();
        }) : '';
    };
  }]);
