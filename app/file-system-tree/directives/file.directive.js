'use strict';

angular.
  module('fileSystemTree').
  directive('file', function() {
    return {
      restrict: 'E',
      templateUrl: 'file-system-tree/directives/file.template.html',
      scope: {
        file: '=',
        deleteFile: '&',
        updateParentPrice: '&',
        updateParentCheckbox: '&'
      },
      controller: ['$rootScope', '$scope', 'selectedFile',
        function($rootScope, $scope, selectedFile) {
          orderChildren();

          $scope.selectedFile = selectedFile;

          $scope.isFolder = $scope.file.type === 'folder';
          $scope.opened = false;
          $scope.editing = false;

          $scope.toggleFolder = function() {
            if (!$scope.isFolder) { return; }
            $scope.opened = !$scope.opened;
          };

          $scope.toggleEditing = function() {
            $scope.editing = !$scope.editing;

            if ($scope.editing) {
              $scope.oldName = $scope.file.name;
              $scope.oldPrice = $scope.file.price;
            } else if (!$scope.isFolder) {
              $scope.updateParentPrice();
            }
          };

          $scope.addToChildren = function(type) {
            var isFolder = type === 'folder';
            $scope.file.children.push({
              name: 'New ' + type,
              createdAt: new Date(),
              type: type,
              editable: true,
              price: isFolder ? 0 : 10,
              children: isFolder ? [] : null
            });

            $scope.updatePrice();
            orderChildren();
          };

          $scope.deleteFromChildren = function(index) {
            $scope.file.children.splice(index, 1);
            $scope.updatePrice();
            emitTreeUpdated();
          };

          $scope.$watchCollection('file', function() {
            $scope.updatePrice();
            emitTreeUpdated();
          });

          $scope.$watch('file.checked', function() {
            if ($scope.updateParentCheckbox) { $scope.updateParentCheckbox(); }
          });

          $scope.updateCheckbox = function(ev) {
            // if event was passed then this function fires by click
            if (ev) {
              // so if file type is folder then update children according to new value
              if ($scope.isFolder) {
                $scope.file.children = updateAllChildren($scope.file.children, ev.target.checked);
              }

              $scope.file.checked = ev.target.checked;
            } else if ($scope.isFolder) {
              // if event wasn't passed then function fired from children directive
              // so children should not be updated

              // checkbox will be indeterminate when among children will be both checked and unchecked files
              $scope.isIndeterminate =
                $scope.file.children.find(function(file) { return !file.checked; }) &&
                $scope.file.children.find(function(file) { return file.checked; });

              // make folder checked when all children would be checked
              $scope.file.checked =
                $scope.file.children.length === $scope.file.children.filter(function(file) { return file.checked; }).length;
            }
          };

          $scope.updatePrice = function() {
            if (!$scope.isFolder) { return; }
            $scope.file.price = 0;

            $scope.file.children.forEach(function(file) {
              $scope.file.price += Number(file.price);
            });

            if ($scope.updateParentPrice) { $scope.updateParentPrice(); }
          };

          // show folders first
          function orderChildren() {
            $scope.file.children = $scope.file.children ? $scope.file.children.sort(function(childA, childB) {
              return childA.type === 'folder' ? -1 :
                childB.type === 'folder' ? 1 : 0;
            }) : null;
          }

          // recursive function for updating children
          function updateAllChildren(children, value) {
            return children.map(function(file) {
              file.checked = value;

              if (file.children) {
                updateAllChildren(file.children, value);
              }

              return file;
            });
          }

          function emitTreeUpdated() {
            $rootScope.$emit('treeUpdated');
          }
        }
      ]
    };
  });
