'use strict';

angular.
  module('fileSystemTree').
  component('fileSystemTree', {
    templateUrl: 'file-system-tree/file-system-tree.template.html',
    controller: [
      '$rootScope',
      '$scope',
      '$sce',
      'localStorageService',
      'treeName',
      'defaultFileSystemTree',
      'payMe',
      function($rootScope, $scope, $sce, localStorageService, treeName, defaultFileSystemTree, payMe) {
        if (!localStorageService.get(treeName)) {
          localStorageService.set(treeName, defaultFileSystemTree);
        }

        $scope.tree = localStorageService.get(treeName);

        $scope.makePayment = function() {
          var priceInCents = $scope.totalPrice * 100;
          payMe.makePayment(priceInCents).then(function(data) {
            if (data.data.status_code === 0) {
              $scope.payMeSaleUrl = $sce.trustAsResourceUrl(data.data.sale_url);
            }
          }, function(err) {
            console.log(err);
          });
        };

        $scope.cancelPayment = function() {
          $scope.payMeSaleUrl = null;
        };

        $rootScope.$on('treeUpdated', function() {
          localStorageService.set(treeName, $scope.tree);

          $scope.totalPrice = calculatePrice($scope.tree);
        });

        // recursive function for calculating price of all selected files
        function calculatePrice(tree) {
          var total = 0;

          if (tree.checked) {
            total += tree.price;
          } else if (tree.children) {
            tree.children.forEach(function(file) {
              total += calculatePrice(file);
            });
          }

          return total;
        }
      }
    ]
  });
