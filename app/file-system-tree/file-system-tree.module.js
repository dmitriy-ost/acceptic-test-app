'use strict';

angular.
  module('fileSystemTree', []).
  constant('treeName', 'fileSystemTree').
  constant('defaultFileSystemTree', {
    name: 'Root',
    createdAt: new Date(),
    type: 'folder',
    editable: false,
    price: 20,
    children: [{
      name: 'Foo',
      createdAt: new Date(),
      type: 'folder',
      editable: true,
      price: 10,
      children: [{
        name: 'Bar',
        createdAt: new Date(),
        type: 'file',
        editable: true,
        price: 10
      }]
    }, {
      name: 'Hello world',
      createdAt: new Date(),
      type: 'file',
      editable: true,
      price: 10
    }]
  });
