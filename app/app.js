'use strict';

angular.module('accepticTestApp', [
  'ngAnimate',
  'fileSystemTree',
  'fileDetails',
  'services',
  'filters',
  'LocalStorageModule',
  'ui.indeterminate'
]);
